![banner](https://banners.beyondco.de/Laravel%20Moodle.png?theme=light&packageManager=composer+require&packageName=cadix%2Flaravel-moodle&pattern=bevelCircle&style=style_1&description=A+Laravel+wrapper+package+for+Moodle+API&md=1&showWatermark=1&fontSize=125px&images=academic-cap)

[![pipeline status](https://gitlab.com/cadix/web/laravel-moodle/badges/main/pipeline.svg)](https://gitlab.com/cadix/web/laravel-moodle/-/commits/main)
[![coverage report](https://gitlab.com/cadix/web/laravel-moodle/badges/main/coverage.svg)](https://gitlab.com/cadix/web/laravel-moodle/-/commits/main)

[[_TOC_]]
# Installation
Run the command below to install the package:

```shell
composer require cadix/laravel-moodle
```

## Config
Run the command below to publish the configuration or override the config in your `.env` file.

```shell
php artisan vendor:publish --provider="Cadix\LaravelMoodle\LaravelMoodleServiceProvider" --tag="config"
```

| env | Default | Description |
| :- | :- | :- |
| MOODLE_HOST | null | Base URI  |
| MOODLE_TOKEN | null | Token from Moodle |
| MOODLE_URI | "${MOODLE_HOST}/moodle/webservice/rest/server.php?wstoken=${MOODLE_TOKEN}&moodlewsrestformat=json&wsfunction=" | Should be complete URI from which methods can be run |

Read the [docs](https://docs.moodle.org/310/en/Using_web_services) on how to get a token.

# Available methods

**Note** not all methods might be available to you. 
Check your Moodle version and the provided [methods](https://docs.moodle.org/dev/Web_service_API_functions) to see if you can use a method.
We aim to be compatible with the latest LTS version according to [Moodles lifecycle](https://docs.moodle.org/dev/Releases#Version_support).


## Auth

| Method | Version | |
| :- | -: | - |
| auth_email_signup_user |  ^3.2 | *Only available when users are allowed to register |
| create |  | wrapper for `auth_email_signup_user` |
| core_auth_request_password_reset | ^3.4 |  |
| core_auth_resend_confirmation_email | ^3.6 | *Only for unconfirmed users |

## Course

| Method | Version | |
| :- | -: | - |
| core_course_get_courses |  ^2.0 | |
| core_course_get_courses_by_field | ^3.2 | |

## Enrol

| Method | Version | |
| :- | -: | - |
| core_enrol_get_enrolled_users | ^2.1 | |
| core_enrol_get_users_courses | ^2.0 | |
| enrol_manual_enrol_users | ^2.0 | |

## User

| Method | Version | |
| :- | -: | - |
| core_user_create_users | ^2.0 | |
| core_user_delete_users | ^2.0 | | 
| core_user_exists | own method | Will check if a user exists; returns bool |
| core_user_first_or_create | own method | Will check if a user exists based on email and or username; returns created or found user |
| core_user_get_users | ^2.5 | |
| core_user_get_users_by_field | ^2.5 | |
| core_user_update_users | ^2.0 | |



