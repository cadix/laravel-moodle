<?php

namespace Cadix\LaravelMoodle\Facades;

use Cadix\LaravelMoodle\User as RootUser;
use Illuminate\Support\Facades\Facade;

/**
 * @mixin RootUser
 *
 * @method static array|null core_user_create_users(array $users)
 * @method static array|null create(array $user)
 * @method static array|null createMany(array $users)
 * @method static bool core_user_delete_users(array $users)
 * @method static bool delete(array|int $users)
 * @method static bool core_user_exists(array $field)
 * @method static array firstOrCreate(string $field, array $user)
 * @method static array|null core_user_get_users(array $fields)
 * @method static array|null all(array $fields)
 * @method static object|null core_user_get_users_by_field(array $fields)
 * @method static array|null find(string|int $value, string $field = 'id')
 * @method static bool core_user_update_users(array $users)
 * @method static array|null courses(int $user_id)
 */
class User extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return RootUser::class;
    }
}
