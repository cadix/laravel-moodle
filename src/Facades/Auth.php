<?php

namespace Cadix\LaravelMoodle\Facades;

use Cadix\LaravelMoodle\Auth as RootAuth;
use Illuminate\Support\Facades\Facade;

/**
 * @mixin RootAuth
 *
 * @method static array|null auth_email_signup_user(array $user)
 * @method static array|null create(array $user)
 * @method static bool core_auth_request_password_reset(string $field, string $value)
 * @method static array|null core_auth_resend_confirmation_email(string $username, string|null $password = null, string|null $redirect_uri = null)
 */
class Auth extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return RootAuth::class;
    }
}
