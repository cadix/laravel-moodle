<?php

namespace Cadix\LaravelMoodle\Tests\Feature;

use Cadix\LaravelMoodle\Facades\User;
use Cadix\LaravelMoodle\Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Http;

class UserTest extends TestCase
{
    use WithFaker;

    public function test_i_can_create_a_single_user(): void
    {
        $data = [
            'createpassword' => 0,
            'password'       => '!This1sJustAT3est',
            'username'       => 'gerlach.brittany77',
            'firstname'      => $this->faker->firstName,
            'lastname'       => $this->faker->lastName,
            'email'          => $this->faker->unique(true)->safeEmail,
            'idnumber'       => $this->faker->unique(true)->randomNumber(),
        ];

        Http::fake([
            config('moodle.host').'/*' => Http::response(file_get_contents( __DIR__ . '/../_responses/user/create_single.json' ), 200, [ 'Content-Type' => 'application/json']),
        ]);

        $user = User::create($data);

        $url = config('moodle.uri') .
            'core_user_create_users'.
            '&users%5B0%5D%5Bcreatepassword%5D='.$data['createpassword'].
            '&users%5B0%5D%5Bpassword%5D='.$data['password'].
            '&users%5B0%5D%5Busername%5D='.$data['username'].
            '&users%5B0%5D%5Bfirstname%5D='.$data['firstname'].
            '&users%5B0%5D%5Blastname%5D='.$data['lastname'].
            '&users%5B0%5D%5Bemail%5D='.$data['email'].
            '&users%5B0%5D%5Bidnumber%5D='.$data['idnumber'];

        Http::assertSent(function (Request $request) use ($url) {
            return $request->url() === $url &&
                $request->method() === 'POST';
        });

        $this->assertIsArray($user);
        $this->assertEquals($user[ 'username' ], $data[ 'username' ]);
    }

    public function test_i_can_create_multiple_users(): void
    {
        $data = [
            [
                'createpassword' => 0,
                'password'       => '!This1sJustAT3est',
                'username'       => 'schuppe.dallas3139',
                'firstname'      => $this->faker->firstName,
                'lastname'       => $this->faker->lastName,
                'email'          => $this->faker->unique(true)->safeEmail,
                'idnumber'       => $this->faker->unique(true)->randomNumber(),
            ],
            [
                'createpassword' => 0,
                'password'       => '!This1sJustAT3est',
                'username'       => 'ksauer368',
                'firstname'      => $this->faker->firstName,
                'lastname'       => $this->faker->lastName,
                'email'          => $this->faker->unique(true)->safeEmail,
                'idnumber'       => $this->faker->unique(true)->randomNumber(),
            ],
        ];

        Http::fake([
            config('moodle.host').'/*' => Http::response(file_get_contents( __DIR__ . '/../_responses/user/create_many.json' ), 200, [ 'Content-Type' => 'application/json']),
        ]);

        $users = User::createMany($data);


        $url = config('moodle.uri') .
            'core_user_create_users'.
            '&users%5B0%5D%5Bcreatepassword%5D='.$data[0]['createpassword'].
            '&users%5B0%5D%5Bpassword%5D='.$data[0]['password'].
            '&users%5B0%5D%5Busername%5D='.$data[0]['username'].
            '&users%5B0%5D%5Bfirstname%5D='.$data[0]['firstname'].
            '&users%5B0%5D%5Blastname%5D='.$data[0]['lastname'].
            '&users%5B0%5D%5Bemail%5D='.$data[0]['email'].
            '&users%5B0%5D%5Bidnumber%5D='.$data[0]['idnumber'].
            '&users%5B1%5D%5Bcreatepassword%5D='.$data[1]['createpassword'].
            '&users%5B1%5D%5Bpassword%5D='.$data[1]['password'].
            '&users%5B1%5D%5Busername%5D='.$data[1]['username'].
            '&users%5B1%5D%5Bfirstname%5D='.$data[1]['firstname'].
            '&users%5B1%5D%5Blastname%5D='.$data[1]['lastname'].
            '&users%5B1%5D%5Bemail%5D='.$data[1]['email'].
            '&users%5B1%5D%5Bidnumber%5D='.$data[1]['idnumber'];

        Http::assertSent(function (Request $request) use ($url) {
            return $request->url() === $url &&
                $request->method() === 'POST';
        });

        $this->assertIsArray($users);
        $this->assertEquals($users[ 0 ][ 'username' ], $data[ 0 ][ 'username' ]);
        $this->assertEquals($users[ 1 ][ 'username' ], $data[ 1 ][ 'username' ]);
    }

    public function test_it_can_delete_one_user(): void
    {
        Http::fake([
            config('moodle.host').'/*' => Http::response(null, 200, ['Content-Type' => 'application/json']),
        ]);

        $id = 11;
        $this->assertTrue(User::delete($id));

        $url = config('moodle.uri') .
            'core_user_delete_users'.
            '&userids%5B0%5D='.$id;

        Http::assertSent(function (Request $request) use ($url) {
            return $request->url() === $url &&
                $request->method() === 'POST';
        });
    }

    public function test_i_can_delete_multiple_users(): void
    {
        Http::fake([
            config('moodle.host').'/*' => Http::response(null, 200, ['Content-Type' => 'application/json']),
        ]);

        $this->assertTrue(User::delete([9, 10]));

        $url = config('moodle.uri') .
            'core_user_delete_users'.
            '&userids%5B0%5D=9&userids%5B1%5D=10';

        Http::assertSent(function (Request $request) use ($url) {
            return $request->url() === $url &&
                $request->method() === 'POST';
        });
    }

    public function test_it_can_get_all_users(): void
    {
        Http::fake([
            config('moodle.host').'/*' => Http::response(file_get_contents( __DIR__ . '/../_responses/user/all.json' ), 200, [ 'Content-Type' => 'application/json']),
        ]);

        $id = 7;
        $users = User::all(['id' => $id]);

        $url = config('moodle.uri') .
            'core_user_get_users'.
            '&criteria%5B0%5D%5Bkey%5D=id&criteria%5B0%5D%5Bvalue%5D='.$id;

        Http::assertSent(function (Request $request) use ($url) {
            return $request->url() === $url &&
                $request->method() === 'POST';
        });

        $this->assertIsArray($users);
        $this->assertEquals($users[ 0 ][ 'id' ], $id);
    }

    public function test_it_can_find_user_by_id(): void
    {
        Http::fake([
            config('moodle.host').'/*' => Http::response(file_get_contents( __DIR__ . '/../_responses/user/find.json' ), 200, [ 'Content-Type' => 'application/json']),
        ]);

        $id = 6;
        $user = User::find($id);

        $url = config('moodle.uri') .
            'core_user_get_users_by_field'.
            '&field=id&values%5B0%5D='.$id;

        Http::assertSent(function (Request $request) use ($url) {
            return $request->url() === $url &&
                $request->method() === 'POST';
        });

        $this->assertIsArray($user);
        $this->assertEquals($id, $user[ 'id' ]);
    }

    public function test_it_can_check_if_user_exists(): void
    {
        Http::fake([
            config('moodle.host').'/*' => Http::response(file_get_contents( __DIR__ . '/../_responses/user/all.json' ), 200, [ 'Content-Type' => 'application/json']),
        ]);

        $username = 'cthompson19';
        $exists = User::exists(['username' => $username]);

        $url = config('moodle.uri') .
            'core_user_get_users_by_field'.
            '&field=username&values%5B0%5D='.$username;

        Http::assertSent(function (Request $request) use ($url) {
            return $request->url() === $url &&
                $request->method() === 'POST';
        });

        $this->assertIsBool($exists);
        $this->assertTrue($exists);
    }

    public function test_it_can_check_if_user_does_not_exist(): void
    {
        Http::fake([
            config('moodle.host').'/*' => Http::response(null, 200, ['Content-Type' => 'application/json']),
        ]);

        $username = 'cthompson18';
        $exists = User::exists(['username' => $username]);

        $url = config('moodle.uri') .
            'core_user_get_users_by_field'.
            '&field=username&values%5B0%5D='.$username;

        Http::assertSent(function (Request $request) use ($url) {
            return $request->url() === $url &&
                $request->method() === 'POST';
        });

        $this->assertIsBool($exists);
        $this->assertFalse($exists);
    }

    public function test_it_can_update_a_user(): void
    {
        Http::fake([
            config('moodle.host').'/*' => Http::response(null, 200, ['Content-Type' => 'application/json']),
        ]);

        $data = [
            'id'        => 7,
            'firstname' => 'Klaas',
        ];
        $updated = User::update($data);

        $url = config('moodle.uri') .
            'core_user_update_users'.
            '&users%5B0%5D%5Bid%5D='.$data['id'].
            '&users%5B0%5D%5Bfirstname%5D='.$data['firstname'];

        Http::assertSent(function (Request $request) use ($url) {
            return $request->url() === $url &&
                $request->method() === 'POST';
        });

        $this->assertIsBool($updated);
        $this->assertTrue($updated);
    }

    public function test_on_first_or_create_it_creates_new_user_if_user_does_not_exist(): void
    {
        $new_user = [
            'createpassword' => 0,
            'password'       => '!This1sJustAT3est',
            'username'       => 'gerlach.brittany77',
            'firstname'      => $this->faker->firstName,
            'lastname'       => $this->faker->lastName,
            'email'          => $this->faker->unique(true)->safeEmail,
            'idnumber'       => $this->faker->unique(true)->randomNumber(),
        ];

        Http::fake([
            config('moodle.host').'/*' => Http::sequence()
                ->push(null, 200, ['Content-Type' => 'application/json']) // check user exists
                ->push(file_get_contents( __DIR__ . '/../_responses/user/create_single.json' ), 200, [ 'Content-Type' => 'application/json'])// create user
                ->push(file_get_contents( __DIR__ . '/../_responses/user/find.json' ), 200, [ 'Content-Type' => 'application/json']),// find user
        ]);

        $user = User::firstOrCreate('username', $new_user);

        $checkUserExistsUrl = config('moodle.uri') .
            'core_user_get_users_by_field'.
            '&field=username&values%5B0%5D='.$new_user['username'];
        $createUserUrl = config('moodle.uri') .
            'core_user_create_users'.
            '&users%5B0%5D%5Bcreatepassword%5D='.$new_user['createpassword'].
            '&users%5B0%5D%5Bpassword%5D='.$new_user['password'].
            '&users%5B0%5D%5Busername%5D='.$new_user['username'].
            '&users%5B0%5D%5Bfirstname%5D='.$new_user['firstname'].
            '&users%5B0%5D%5Blastname%5D='.$new_user['lastname'].
            '&users%5B0%5D%5Bemail%5D='.$new_user['email'].
            '&users%5B0%5D%5Bidnumber%5D='.$new_user['idnumber'];
        $findUserUrl = config('moodle.uri') .
            'core_user_get_users_by_field'.
            '&field=username&values%5B0%5D='.$new_user['username'];

        Http::assertSent(function (Request $request) use ($checkUserExistsUrl, $createUserUrl, $findUserUrl) {
            return ($request->url() === $checkUserExistsUrl && $request->method() === 'POST') ||
                ($request->url() === $createUserUrl && $request->method() === 'POST') ||
                    ($request->url() === $findUserUrl && $request->method() === 'POST');
        });

        $this->assertIsArray($user);
        $this->assertEquals($new_user[ 'username' ], $user[ 'username' ]);
    }

    public function test_on_first_or_create_it_does_not_create_new_user_if_user_exist(): void
    {
        $new_user = [
            'createpassword' => 0,
            'password'       => '!This1sJustAT3est',
            'username'       => 'gerlach.brittany77',
            'firstname'      => $this->faker->firstName,
            'lastname'       => $this->faker->lastName,
            'email'          => $this->faker->unique(true)->safeEmail,
            'idnumber'       => $this->faker->unique(true)->randomNumber(),
        ];

        Http::fake([
            config('moodle.host').'/*' => Http::sequence()
                ->push(file_get_contents( __DIR__ . '/../_responses/user/find.json' ), 200, [ 'Content-Type' => 'application/json']) // check user exists
                ->push(file_get_contents( __DIR__ . '/../_responses/user/find.json' ), 200, [ 'Content-Type' => 'application/json']),// find user
        ]);

        $user = User::firstOrCreate('username', $new_user);

        $checkUserExistsUrl = config('moodle.uri') .
            'core_user_get_users_by_field'.
            '&field=username&values%5B0%5D='.$new_user['username'];
        $findUserUrl = config('moodle.uri') .
            'core_user_get_users_by_field'.
            '&field=username&values%5B0%5D='.$new_user['username'];

        Http::assertSent(function (Request $request) use ($checkUserExistsUrl, $findUserUrl) {
            return ($request->url() === $checkUserExistsUrl && $request->method() === 'POST') ||
                ($request->url() === $findUserUrl && $request->method() === 'POST');
        });

        $this->assertIsArray($user);
        $this->assertEquals($new_user[ 'username' ], $user[ 'username' ]);
    }

    public function test_it_can_get_courses_a_user_is_enrolled_in(): void
    {
        Http::fake([
            config('moodle.host').'/*' => Http::response(file_get_contents( __DIR__ . '/../_responses/enrol/courses.json' ), 200, [ 'Content-Type' => 'application/json']),
        ]);

        $id = 4;
        $courses = User::courses($id);

        $url = config('moodle.uri') .
            'core_enrol_get_users_courses'.
            '&userid='.$id;

        Http::assertSent(function (Request $request) use ($url) {
            return $request->url() === $url &&
                $request->method() === 'POST';
        });

        $this->assertIsArray($courses);
        $this->assertContains($id, array_column($courses, 'id'));
    }
}
