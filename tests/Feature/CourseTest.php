<?php

namespace Cadix\LaravelMoodle\Tests\Feature;

use Cadix\LaravelMoodle\Facades\Course;
use Cadix\LaravelMoodle\Tests\TestCase;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Http;

class CourseTest extends TestCase
{
    public function test_i_can_get_all_courses(): void
    {
        Http::fake([
            config('moodle.host').'/*' => Http::response(file_get_contents( __DIR__ . '/../_responses/course/all.json' ), 200, [ 'Content-Type' => 'application/json']),
        ]);

        $courses = Course::all();

        $url = config('moodle.uri') .
            'core_course_get_courses';

        Http::assertSent(function (Request $request) use ($url) {
            return $request->url() === $url &&
                $request->method() === 'POST';
        });

        $this->assertIsArray($courses);
    }

    public function test_i_can_get_multiple_courses_by_id(): void
    {
        Http::fake([
            config('moodle.host').'/*' => Http::response(file_get_contents( __DIR__ . '/../_responses/course/all.json' ), 200, [ 'Content-Type' => 'application/json']),
        ]);

        $courses = Course::all([1, 2]);

        $url = config('moodle.uri') .
            'core_course_get_courses&options%5Bids%5D%5B0%5D=1&options%5Bids%5D%5B1%5D=2';

        Http::assertSent(function (Request $request) use ($url) {
            return $request->url() === $url &&
                $request->method() === 'POST';
        });

        $this->assertIsArray($courses);
        $this->assertContains(1, array_column($courses, 'id'));
        $this->assertContains(2, array_column($courses, 'id'));
    }

    public function test_it_can_find_courses_by_id(): void
    {
        Http::fake([
            config('moodle.host').'/*' => Http::response(file_get_contents( __DIR__ . '/../_responses/course/find.json' ), 200, [ 'Content-Type' => 'application/json']),
        ]);

        $id = 4;
        $course = Course::find($id);

        $url = config('moodle.uri') .
            'core_course_get_courses_by_field&field=id&value=' . $id;

        Http::assertSent(function (Request $request) use ($url) {
            return $request->url() === $url &&
                $request->method() === 'POST';
        });

        $this->assertIsArray($course);
        $this->assertEquals($id, $course[ 'id' ]);
    }

    public function test_it_can_get_enrolled_users(): void
    {
        Http::fake([
            config('moodle.host').'/*' => Http::response(file_get_contents( __DIR__ . '/../_responses/enrol/users.json' ), 200, [ 'Content-Type' => 'application/json']),
        ]);

        $id = 4;
        $users = Course::users($id);

        $url = config('moodle.uri') .
            'core_enrol_get_enrolled_users&courseid=' . $id;

        Http::assertSent(function (Request $request) use ($url) {
            return $request->url() === $url &&
                $request->method() === 'POST';
        });

        $this->assertIsArray($users);
        $this->assertContains($id, array_column($users[ 0 ][ 'enrolledcourses' ], 'id'));
    }
}
