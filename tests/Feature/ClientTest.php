<?php

namespace Cadix\LaravelMoodle\Tests\Feature;

use Cadix\LaravelMoodle\Facades\User;
use Cadix\LaravelMoodle\Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Http;

class ClientTest extends TestCase
{
    use WithFaker;

    public function test_on_error_it_will_not_concatenate_urls(): void
    {
        $new_user = [
            'createpassword' => 0,
            'password'       => '!This1sJustAT3est',
            'username'       => 'gerlach.brittany77',
            'firstname'      => $this->faker->firstName,
            'lastname'       => $this->faker->lastName,
            'email'          => $this->faker->unique(true)->safeEmail,
            'idnumber'       => $this->faker->unique(true)->randomNumber(),
        ];

        Http::fake([
            config('moodle.host').'/*' => Http::sequence()
                ->push(null, 422, ['Content-Type' => 'application/json']) // Unprocessable Entity
                ->push(null, 200, ['Content-Type' => 'application/json']) // Check if user exists
                ->push(file_get_contents( __DIR__ . '/../_responses/user/create_single.json' ), 200, [ 'Content-Type' => 'application/json']) // create user
                ->push(file_get_contents( __DIR__ . '/../_responses/user/find.json' ), 200, [ 'Content-Type' => 'application/json'])// find user
        ]);

        $user = User::firstOrCreate('username', $new_user);

        Http::assertNotSent(function (Request $request) use ($new_user) {
            return $request->method() === 'POST' &&
                str_contains($request->url(), '&users%5B0%5D%5Bidnumber%5D='.$new_user['idnumber'].'https'); // Only check if not concatenated
        });

        Http::assertNotSent(function (Request $request) {
            return $request->method() === 'POST' &&
                str_contains($request->url(), 'wsfunction=https'); // Only check if not concatenated
        });

        Http::assertSent(function (Request $request) {
            return $request->method() === 'POST' &&
                str_contains($request->url(), 'wsfunction=core_user_get_users_by_field');
        });
    }
}
