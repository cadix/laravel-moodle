<?php

namespace Cadix\LaravelMoodle\Tests;

use Cadix\LaravelMoodle\LaravelMoodleServiceProvider;
use Illuminate\Foundation\Bootstrap\LoadEnvironmentVariables;

class TestCase extends \Orchestra\Testbench\TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    protected function getPackageProviders($app): array
    {
        return [
            LaravelMoodleServiceProvider::class,
        ];
    }

    protected function getEnvironmentSetUp($app): void
    {
        $app->useEnvironmentPath(__DIR__ . '/..');
        $app->bootstrapWith([LoadEnvironmentVariables::class]);
        parent::getEnvironmentSetUp($app);
        $app[ 'config' ]->set('moodle.token', env('MOODLE_TOKEN'));
        $app[ 'config' ]->set('moodle.host', env('MOODLE_HOST'));
        $app[ 'config' ]->set('moodle.uri', env('MOODLE_URI'));
        $app[ 'config' ]->set('moodle.retries', env('MOODLE_RETIRES'));
        $app[ 'config' ]->set('moodle.timeout', env('MOODLE_TIMEOUT'));
    }
}
