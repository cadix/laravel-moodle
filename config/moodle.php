<?php

return [
    'host'    => env('MOODLE_HOST'),
    'token'   => env('MOODLE_TOKEN'),
    'uri'     => env('MOODLE_URI'),
    'retries' => env('MOODLE_RETRIES', 3),
    'timeout' => env('MOODLE_TIMEOUT', 2), // in seconds
];
